const readline = require('readline');

var io = require('socket.io-client');
const socket = io('http://localhost:3000');

readline.emitKeypressEvents(process.stdin);
process.stdin.setRawMode(true);

var name = '';
var tmp = '';

socket.on('chat', function (data) {
    process.stdout.clearLine();
    process.stdout.cursorTo(0);
    console.log(data);
    process.stdout.write(tmp)
});

process.stdin.on('keypress', (char, key) => {
    if (key.ctrl && key.name === 'c') {
        process.exit();
    }
    else if (key.name === 'return') {
        if (name === '') {
            name = tmp
            tmp = '';
            console.log();
            socket.emit('chat', name + ' is connected!');
        }
        else {
            process.stdout.clearLine();
            process.stdout.cursorTo(0);
            socket.emit('chat', name + ': ' + tmp);
            tmp = '';
        }
    }
    else if (key.name === 'backspace') {
        tmp = tmp.slice(0, -1);
        process.stdout.clearLine();
        process.stdout.cursorTo(0);
        process.stdout.write(tmp)
    }
    else {
        process.stdout.write(char);
        tmp += char;
    }
});

console.log('Your name: ');
